/*
Author: de Freitas, P.C.P
Description - Binary Search
*/
#include<iostream>
#include<vector>
#include<cstdio>
using namespace std;
int binarySearch(const vector<int> & v, int element)
{
	int low = 0;//limite inferior;
	int right = v.size()-1;//limite superior;
	int middle;
	while (low<=right)
	{
		middle = (low + right) / 2;
		if (element == v[middle])
		{
			printf("[%i] = %i \n",middle,v[middle]);
			return middle;
		}
		if (element < v[middle])
		{
			right = middle - 1;
		}
		else
		{
			low = middle + 1;
		}
	}
	printf("Not found\n");
	return -1;//not found;
}
int main()
{
	vector<int> nums{ 1, 3, 5, 7 };
	binarySearch(nums, 0);
	binarySearch(nums, 2);
	binarySearch(nums, 5);
	return 0;
}